export class Movie {
    id: number;
    title: string;
    description: string;
    stars: number;
    year: number;
    image: string;
    directors: string;
    actors: string;
}
